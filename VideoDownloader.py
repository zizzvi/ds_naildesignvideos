
import os
import sys
import re
import time
import colorama

from colorama import Fore, Back, Style
from urllib.request import urlopen, urlretrieve

colorama.init()

baseURL = "https://vk.com/video-126100310_4562"
startingAt = 39017
endingAt = 39028 #43790
count = 0

def saveVideo(url, path):
    def reporthook(blocknum, blocksize, totalsize):
        readsofar = blocknum * blocksize
        if totalsize > 0:
            percent = readsofar * 1e2 / totalsize
            s = "\r%5.1f%% %*d / %d" % (percent, len(str(totalsize)), readsofar, totalsize)
            sys.stderr.write(s)
            if readsofar >= totalsize:
                sys.stderr.write("\n")
        else:
            sys.stderr.write("read %d\n" % (readsofar,))

    dest, _ = urlretrieve(url, path, reporthook=reporthook)
    return dest 


def videoDownloader(startingAt, endingAt, count):
    if startingAt <= endingAt:
        videoURL = baseURL + str(startingAt)

        print(Fore.CYAN + "Video URL : " + Fore.GREEN  + Style.BRIGHT + videoURL + Style.RESET_ALL)
        print("Retriving...")
        
        f = urlopen(videoURL)
        source = f.read().decode("windows-1251")
        reg = re.compile('<source src=\\\\"([^"]*)\\\\"')
        urls = reg.findall(source)

        for i in ['360.mp4', '240.mp4']:
            for url in urls:
                if i in url:
                    source = url.replace('\\/', '/')
                    reg = re.compile('/([^/]*\.mp4)')
                    name = str(startingAt) + "." + str(i)
                    saveVideo(source, name)
                   
                    print(Fore.RED + Style.BRIGHT + "Saved as " + name)
                    print(Style.RESET_ALL)
                    
                    startingAt += 1
                    count += 1
                    videoDownloader(startingAt, endingAt, count)

    print(Fore.MAGENTA + Style.BRIGHT + "All Done \nDownloaded " + str(count) + " Videos")
    sys.exit(2)

if __name__ == '__main__':
    videoDownloader(startingAt, endingAt, count)
